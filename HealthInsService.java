package comhealth;

import java.util.ArrayList;
import java.util.List;

import com.InsurerPerson;
import comconstant.BaseVarConstants;
import comhealth.PremiumCalService;

public class HealthInsService{
	
	public static void main( String[] args )
    {
        InsurerPerson insurer = new InsurerPerson();
        insurer.setName("Norman Gomes");
        insurer.setAge(34);
        insurer.setGender(BaseVarConstants.MALE);
        insurer.setBloodPressure(BaseVarConstants.NO);
        insurer.setBloodSugar(BaseVarConstants.NO);
        insurer.setHypertension(BaseVarConstants.NO);
        insurer.setOverweight(BaseVarConstants.YES);
        
        List<String> welHabits=new ArrayList<String>();
        welHabits.add("dailyExcercise");
        insurer.setWelHabits(welHabits);
        List<String> notWelHabits=new ArrayList<String>();
        notWelHabits.add("alcohol");
        insurer.setNotWelHabits(notWelHabits);
        
        PremiumCalService service =new PremiumCalService();
        
        double PremiumValue = BaseVarConstants.basePremium;
        
        PremiumValue = service.IncreaseOnAge(insurer.getAge(),PremiumValue);
        
        PremiumValue = service.IncreaseOnGender(insurer.getGender(),PremiumValue);
        
        PremiumValue = service.IncreaseOnCurrentHealthStatus(insurer,PremiumValue);
        
        PremiumValue = service.IncreaseOnHabits(insurer,PremiumValue);
        
        System.out.println("Insurance for "+ insurer.getName()+" = " + PremiumValue);
        
    }

}
