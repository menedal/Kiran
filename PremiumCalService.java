package comhealth;

import com.InsurerPerson;
import comconstant.BaseVarConstants;

public class PremiumCalService {
	
public double IncreaseOnAge(int age,double amount){
		
		if( age<=0 ){ // because for age less than 0 we can't proceed.
			return 0;
		}
		else if(age<18){
			return BaseVarConstants.basePremium; //base Premium 5000 amount
		}
		else if(age>=18 && age<40){  //because from age 18 to 40 increase in % is same 10%.
			return IncrementedAmount(age,0.1,amount);  //10% increase
		}else{
			return IncrementedAmount(age,0.2,amount);  //20% increase
		}
	}

	private double IncrementedAmount(int age,double per,double amount){
		int increase= (age-18)/5;	//because its every 5 years		
		for(int i=0;i<increase;i++){
			amount+=amount*per;
		}
		return amount;
	}

	public double IncreaseOnCurrentHealthStatus(InsurerPerson insurer,double amount) {
		if(insurer.getHypertension().equals(BaseVarConstants.YES)){
			amount+=amount*0.01;
		}
		if(insurer.getBloodPressure().equals(BaseVarConstants.YES)){
			amount+=amount*0.01;
		}
		if(insurer.getBloodSugar().equals(BaseVarConstants.YES)){
			amount+=amount*0.01;
		}
		if(insurer.getOverweight().equals(BaseVarConstants.YES)){
			amount+=amount*0.01;
		}
		return amount;
	}

	public double IncreaseOnGender(String gender, double amount) {
		if(gender.equalsIgnoreCase(BaseVarConstants.MALE)){
			return amount+=amount*0.02;
		}
		return amount;
	}

	public double IncreaseOnHabits(InsurerPerson insurer, double amount) {
		for(int i=0;i<insurer.getWelHabits().size();i++){
			amount-=amount*0.03;
		}
		for(int i=0;i<insurer.getNotWelHabits().size();i++){
			amount+=amount*0.03;
		}
		return amount;
	}

}
