package com;

import java.util.List;

public class InsurerPerson {
	
	private String name;
	
	private String gender;
	
	private int age;
	
	private String Hypertension;
	
	private String bloodPressure;
	
	private String bloodSugar;
	
	private String overweight;
	
	
	private List<String> WelHabits;
	
	private List<String> NotWelHabits;
	
	
	
	public String getName() {
		return name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getHypertension() {
		return Hypertension;
	}
	public void setHypertension(String hypertension) {
		Hypertension = hypertension;
	}
	public String getBloodPressure() {
		return bloodPressure;
	}
	public void setBloodPressure(String bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	public String getBloodSugar() {
		return bloodSugar;
	}
	public void setBloodSugar(String bloodSugar) {
		this.bloodSugar = bloodSugar;
	}
	public String getOverweight() {
		return overweight;
	}
	public void setOverweight(String overweight) {
		this.overweight = overweight;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public List<String> getWelHabits() {
		return WelHabits;
	}
	public void setWelHabits(List<String> welHabits) {
		WelHabits = welHabits;
	}
	public List<String> getNotWelHabits() {
		return NotWelHabits;
	}
	public void setNotWelHabits(List<String> notWelHabits) {
		NotWelHabits = notWelHabits;
	}		

}
